# Arduino Motor Control

A differential drive controller with visual encoders that receives commands via serial interface.

Can be controlled from a host device application such as [this ROS controller](https://github.com/phildue/bot_base)
